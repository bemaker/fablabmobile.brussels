// Note: some RSS feeds can't be loaded in the browser due to CORS security.
// To get around this, you can use a proxy.
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

let parser = new RSSParser();
console.log(parser);
parser.parseURL(CORS_PROXY + 'http://www.bemaker.eu/feed/?post_type=workshops', function(err, feed) {
  console.log(feed.title);
  feed.items.forEach(function(entry) {
    //console.log(entry);
    var content = entry.content;
    var content = content.split("£");
    var date = content[0];
    console.log(date);
    var year = date.split();
    console.log(year);
    var image = content[1];
    var link = entry.link;
    var imageStr = '<a href="' + link + '"><div class="thumb" style="background-image: url(' + image + '); background-size: cover; background-position: center center;"></div></a>';
    var imageDiv = $("<div class=thumb-container>").html(imageStr);
    var title = entry.title;
    var title = '<a href="' + link + '">'+ title + '</a>';
    var titleDiv = $("<div class='title'>").html(title);
    var content = content[2];
    var contentDiv = $("<div class='summary'>").html(content);
    var author = entry.creator;
    var authorDiv = $("<div class='author'>").text(author);
    var date = entry.pubDate;
    console.log(entry);
    var dateDiv = $("<div class='date'>").text(date);
    //var event = imageDiv + titleDiv + contentDiv + authorDiv + dateDiv;
    //console.log(event);
    imageDiv.appendTo($("#events"));
    titleDiv.appendTo($("#events"));
    contentDiv.appendTo($("#events"));
    authorDiv.appendTo($("#events"));
    //dateDiv.appendTo($("#events"));
  });
})

//https://www.bemaker.eu/wp-content/uploads/2018/03/tag_web.jpg