// Note: some RSS feeds can't be loaded in the browser due to CORS security.
// To get around this, you can use a proxy.
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

let parser = new RSSParser();
parser.parseURL(CORS_PROXY + 'http://www.bemaker.eu/feed/?post_type=workshops', function(err, feed) {
  feed.items.forEach(function(entry) {
    var content = entry.content;
    var content = content.split("£");
    var date = content[1];
    var date = date.split("");
    var date = date[0] + date[1] + date[2] + date[3] + "-" + date[4] + date[5] + "-" + date[6] + date[7];
    var date = new Date(date);
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var date = date.toLocaleString("fr-BE", options);
    var image = content[0];
    var link = entry.link;
    var imageStr = '<a href="' + link + '"><div class="thumb" style="background-image: url(' + image + '); background-size: cover; background-position: center center;"></div></a>';
    var imageDiv = $("<div class=thumb-container>").html(imageStr);
    var title = entry.title;
    var title = '<a href="' + link + '">'+ title + '</a>';
    var titleDiv = $("<div class='title'>").html(title);
    var content = content[2];
    var contentDiv = $("<div class='summary'>").html(content);
    var author = entry.creator;
    var authorDiv = $("<div class='author'>").text(author);
    var dateDiv = $("<div class='date'>").text(date);
    imageDiv.appendTo($("#events"));
    titleDiv.appendTo($("#events"));
    dateDiv.appendTo($("#events"));
    contentDiv.appendTo($("#events"));
    authorDiv.appendTo($("#events"));
  });
})

//https://www.bemaker.eu/wp-content/uploads/2018/03/tag_web.jpg